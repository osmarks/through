Discussion topics
=================

In this section, we will go in depth into the topics explored by this
analysis.

.. toctree::
    :maxdepth: 1

    topics/computers
    topics/randomness
    topics/encodings
    topics/devices
    topics/modem-protocols
