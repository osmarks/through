Notable people
==============

In this document, we will reference people who had notable roles in the
development of environments in which thox runs, and/or in major projects
thox interacts with and/or takes inspiration from.

.. _player-dan200:

.. rubric:: Daniel Ratcliffe (dan200)

Creator of the original ComputerCraft mod.

* `dan200.net <http://www.dan200.net/>`_
* `dan200 on the CC forums <https://www.computercraft.info/forums2/index.php?/user/27-dan200/>`_
* `dan200 on Github <https://github.com/dan200>`_

.. _player-squiddev:

.. rubric:: Jonathan Coates (SquidDev)

Author of the CC:T fork.

* `squiddev.cc <https://squiddev.cc/>`_
* `SquidDev on CC forums <https://www.computercraft.info/forums2/index.php?/user/24549-squiddev/>`_
* `SquidDev on CC:T forums <https://forums.computercraft.cc/index.php?action=profile;u=58>`_
* `SquidDev-CC on Github <https://github.com/SquidDev-CC>`_

.. _player-lddestroier:

.. rubric:: LDDestroier

Author of a number of notable programs for native CraftOS; some protocols linked
to their programs are documented in :ref:`modem-ldd`.

* `LDDestroier on CC forums <https://www.computercraft.info/forums2/index.php?/user/28731-lddestroier/>`_
* `LDDestroier on CC:T forums <https://forums.computercraft.cc/index.php?action=profile;u=84>`_
* `LDDestroier on Github <https://github.com/LDDestroier>`_

.. _player-oeed:

.. rubric:: Oliver Cooper (oeed)

Creator of Bedrock, one of the big GUI frameworks back in 2015, and of OneOS,
an OS built on this framework.

* `olivercooper.me <https://olivercooper.me/>`_
* `oeed on CC forums <http://www.computercraft.info/forums2/index.php?/user/12402-oeed/>`_
* `oeed on Github <https://github.com/oeed>`_

.. _player-MightyPirates:

.. rubric:: MightyPirates

OpenComputers creator.

* `MightyPirates on Github <https://github.com/MightyPirates>`_

.. _player-bigshinytoys:

.. rubric:: BigSHinyToys

Creator of the GPS API in original ComputerCraft in 2012, using modem APIs
and trilateration; see :ref:`modem-gps`.

* `BigSHinyToys on CC forums <http://www.computercraft.info/forums2/index.php?/user/932-bigshinytoys/>`_

.. _player-nitrogenfingers:

.. rubric:: nitrogenfingers

Creator of npaintpro and the NFT format.

* `nitrogenfingers on CC forums <http://www.computercraft.info/forums2/index.php?/user/643-nitrogenfingers/>`_
* `nitrogenfingers on Github <https://github.com/nitrogenfingers?tab=repositories>`_

.. _player-lyqyd:

.. rubric:: Lyqyd

Creator of the net shell (nsh).

* `Lyqyd on CC forums <http://www.computercraft.info/forums2/index.php?/user/1736-lyqyd/>`_
* `lyqyd on Github <https://github.com/lyqyd>`_

.. _player-justync7:

.. rubric:: justync7

Creator of Rednoot.

* `justync7 on CC forums <http://www.computercraft.info/forums2/index.php?/user/1364-justync7/>`_
* `Lustyn on Github <https://github.com/Lustyn>`_

.. _player-kepler155c:

.. rubric:: kepler155c

Creator of OPUS OS.

* `kepler on CC forums <https://www.computercraft.info/forums2/index.php?/user/63530-kepler/>`_
* `kepler155c on CC:T forums <https://forums.computercraft.cc/index.php?action=profile;u=92>`_
* `kepler155c on Github <https://github.com/kepler155c>`_

.. _player-anavrins:

.. rubric:: Anavrins

Maintainer of OPUS OS as of 2020.

* `Anavrins on CC forums <https://www.computercraft.info/forums2/index.php?/user/12870-anavrins/>`_
* `Anavrins on CC:T forums <https://forums.computercraft.cc/index.php?action=profile;u=25>`_
* `xAnavrins on Github <https://github.com/xAnavrins>`_

.. _player-jackmacwindows:

.. rubric:: JackMacWindows

Contributor to CC:T, author of a few programs.

* `JackMacWindows on CC forums <https://www.computercraft.info/forums2/index.php?/user/62182-jackmacwindows/>`_
* `JackMacWindows on CC:T forums <https://forums.computercraft.cc/index.php?action=profile;u=210>`_
* `MCJack123 on Github <https://github.com/MCJack123>`_

.. _player-gollark:

.. rubric:: osmarks (gollark)

Author of Skynet and PotatOS.

* `osmarks.net <https://osmarks.net>`_ (previously
  `osmarks.tk <https://osmarks.tk>`_, previously `osmarks.ml <https://osmarks.ml>`_).
* `osmarks on CC:T forums <https://forums.computercraft.cc/index.php?action=profile;u=37>`_.
* `osmarks on Github <https://github.com/osmarks/>`_.
* `git.osmarks.net (own Git hosting) <https://git.osmarks.net/>`_.
* `misc/computercraft scripts <https://github.com/osmarks/misc/tree/master/computercraft>`_.