.. _modem-protocols:

Modem protocols
===============

On top of the modem layer (see :ref:`modem-devices`) lies application
protocols, using some transport, session and presentation layers.
This page mostly describes protocols implemented using ComputerCraft modems
or equivalent, which are broadcast-only networks with very basic mechanisms,
where most protocols are actually made to be between two devices, which
leads to transport protocols introducing addressing on top of the modem API.

Hence, two approaches co-exist:

* The Rednet approach: using port numbers as addresses on a network, with
  protocols being part of the message as a string; see ``sProtocol``
  in `rednet.send`_.
* The “one port, one protocol” approach: using port numbers as protocol
  identifiers, with messages not compatible with Rednet messages.

The first approach is embedded in the ComputerCraft ROM, therefore is
accessible to any computer attached to a modem on any Minecraft server
including the mod. It is the officially recommended way to network.
The second approach has mainly be used by third-party OSes such as OneOS
or OPUS OS, and also has limits which we will describe in this section.

Note however that Rednet is not the only protocol of its kind, and others
have been created either for answering Rednet's identified weaknesses,
or for funsies.

.. _rednet:

Rednet
------

Rednet uses port numbers as adresses, with the exception of two special
port numbers:

* ``65533`` (known as ``rednet.CHANNEL_REPEAT``): the repeat channel, which
  allows physical networks to communicate with each other using physical
  relays.
* ``65535`` (known as ``rednet.CHANNEL_BROADCAST``): the broadcast channel,
  which allows a message to be sent to all computers on all connected
  physical networks (repeaters share these messages as any other Rednet
  message).

Any Rednet message is a table composed of the following members:

* ``nMessageID``: a pseudo-random number in the :math:`[1, 2^{32} - 1]` range.
  This member allows a Rednet-enabled device to recognize the message as
  a Rednet message, and repeaters to mitigate loops by not transmitting
  the same message in a given timespan (currently 30 seconds).
* ``nRecipient``: the ID of the computer to which the message is sent.
* ``message``: the message.
* ``sProtocol``: the protocol as a string or ``nil``.

As a globally unique number, Rednet uses the computer ID, an integer which is
guaranteed to be unique for a given world (NOT server) in the
:math:`[0, 2^{32} - 1]` range. To determine the source of a Rednet message,
the “reply channel” is used by filling it with the computer ID.

.. warning::

    The computer ID can be non-unique in case computers with a given ID
    are duplicated (through cheating or otherwise), although this might
    cause other problems such as intricated filesystems between the
    two computers having the same ID.

    Plus, the computer ID can be spoofed; one way to do this in CraftOS
    is to reimplement rednet over the modem API but getting the ID to use
    through other means, such as having a ``idToSpoof`` constant.

On a Rednet-enabled device, two channels are opened by default for receiving
messages: the broadcast channel, and the modem channel with the computer ID
as the port number, e.g. channel 14 for a computer which ID is 14, for direct
messages or answers; see `rednet.open`_.

.. warning::

    Notice that there is no limit to the ID a computer can be assigned except
    the Java ``int`` type maximum value, which is :math:`2^{32} - 1`, where the
    modem number can only between :math:`0` and :math:`2^{16} - 1`. There are
    two cases that might cause unexpected behaviours:

    * The computer ID exceeds :math:`2^{16} - 1`, which is not a valid port
      number.
    * The computer ID is a reserved Rednet channel number amongst
      65533 (known in CraftOS as ``rednet.CHANNEL_BROADCAST``) and
      65535 (known in CraftOS as ``rednet.CHANNEL_REPEAT``), or even
      65534 (known in CraftOS as ``rednet.CHANNEL_GPS``) for that matter.

    The computer identifier should be lesser or equal to 65532 for it to
    have no unexpected behaviour on a Rednet network, which is NOT GUARANTEED.
    Note that the computer identifier should be unique to the world it is into,
    so that means the computer counter (not computer count) should not exceed
    65532. This should cause an anomaly on servers using ComputerCraft in an
    intense manner, although I have not found the case to have happened on
    forums.

    See `IDAssigner.getNextID`_ and `Java Primitive Types and Values`_.

When receiving a new message, the Rednet event loop checks if the message
is a table and has a ``nMessageID`` member set to understand it as a
Rednet message, which allows the Rednet and the “port protocols” approaches
to coexist on the network; see `rednet.run`_.

.. note::

    This means that when implementing your own protocol not using the Rednet
    API, you should be aware that Rednet messages might arrive on your port
    number; you need to either not use tables as messages, or use tables but
    have a member that allows you to recognize messages in your protocol's
    vocabulary, or at least ignore Rednet messages defining the
    ``nMessageID`` field.

The advantages of using protocols on top of Rednet are the following:

* There is a “hardware” limit to the number of ports you can open on a
  modem: 128 ports, which even if unlikely is still reachable. Using the
  Rednet approach, there is no such arbitrary limit, as all string
  combinations (for the ``sProtocol`` field) are allowed.
* Rednet has a “repeat” system, using the repeat channel; this is
  especially useful for messages on wireless modems, which can travel so
  many blocks at once (64 blocks, or 16 during a storm, in the default
  configuration). With this system, likely to be implemented by other
  players as well on a server, you can have Rednet repeaters, which
  by default behave like hubs (they repeat messages on all other modems,
  but do not optimize traffic by remembering which host is available
  through which modem), with loop mitigation; see the CraftOS
  `repeat command`_.

.. note::

    As repeaters behave as hubs and not switches, this means they don't try
    to optimize communications by trying to determine on which interface a
    Rednet-enabled device is, and always repeat on all interfaces. While this
    means more messages than necessary are exchanged, it also means that
    several computers can use the same computer ID for Rednet, and that no
    packet is lost because of bad routing, which has the nice security
    implication that a device cannot block messages while trying to
    impersonate a device or censor a device without the repeaters being
    destroyed.

.. todo::

    It is suggested that the repeat port only could as well be good enough,
    or at least multiplexing individual channels with a mod by 60000 (e.g.
    devices 5, 60005 and 120005 share the same port and check if the
    ``nRecipient`` is set correctly to their address; an idea I find really
    clever by Lupus590).

    However, this field is not checked in classic Rednet that the
    ``nRecipient`` corresponds to the computer ID, making this approach
    non-retrocompatible, which is a real shame…

    Wojbie suggested that this number is very high anyway, and that servers
    having as many computer IDs as that should use other protocols than
    classic Rednet. As of today, I haven't decided on what to recommend
    in this documentation, or how to describe the case.

    However, Wojbie suggested an evolution of Rednet where messages would
    all be sent and received on ``CHANNEL_REPEAT`` and Rednet messages
    could have a ``nReplyChannel`` field as the modem equivalent, but
    which could go far higher than the channel number; and of course,
    when receiving a modem message, the ``nRecipient`` field is checked,
    on the contrary to classic Rednet. See `their suggestion as a
    valid implementation <https://pastebin.com/RFENcgJD>`_.

See the `rednet reference`_ for more information.

Some known protocols running over Rednet are the following:

.. list-table::
    :widths: 10 100
    :header-rows: 1

    * - sProtocol
      - Name
    * - ``"dns"``
      - Rednet lookup protocol; see :ref:`modem-lookup`.
    * - ``"tror"``
      - Terminal Redirection over Rednet (TRoR); see :ref:`modem-tror`.
    * - ``"REMOTE_PERIPHERAL"``
      - `Remote Peripheral <https://github.com/blunty666/CC-Programs-and-APIs/tree/master/remotePeripheral>`_
        protocol.
    * - ``"MESSAGE_HANDLER"``
      - `Message Handling <https://github.com/blunty666/CC-Programs-and-APIs/tree/master/messageHandler>`_
        protocol, a req/rep transport protocol with subsystem support.

In the next chapters, we'll go more in depth on protocols that use Rednet.

.. toctree::
    :maxdepth: 2

    modem-protocols/lookup
    modem-protocols/tror

Rednet derivatives
------------------

.. todo::

    Find information about the criticism about Rednet or why the newbies
    don't want to use Rednet.

.. todo::

    Find which alternatives to talk about; there are probably some interesting
    and uninteresting ones. Some alternatives I found:

    * `brednet`_: "better rednet" by LDDestroier, didn't figure out the
      differenciating features with basic Rednet yet.
    * `CryptoNet`_: adds encryption and PKI-based trusting.

    Plus alternative systems brought by systems for their built-in protocols:

    * OPUS OS: encryption and peer-to-peer-based trusting.
    * OneOS: ?

.. toctree::

    modem-protocols/lyqyd

Port protocols
--------------

Some protocols do not use Rednet, but the modem channels directly in a UDP
or EtherTypes_ fashion. Messages in such a protocol are either not tables
or tables not defining the ``nMessageID`` for distinguishing messages between
Rednet and non-Rednet, although it is to be noted not all implementations
ignore Rednet messages on the given channels.

Some known port protocols are the following:

.. list-table::
    :widths: 1 10 8
    :header-rows: 1

    * - Channel
      - Name
      - Protocol
    * - 19
      - ``opus-trust``
      - :ref:`modem-opus-trust`
    * - 22
      - ``opus-stelnet``
      - :ref:`modem-opus-telnet`
    * - 23
      - ``opus-telnet``
      - :ref:`modem-opus-telnet`
    * - 139
      - ``opus-samba``
      - :ref:`modem-opus-samba`
    * - 161
      - ``opus-snmp``
      - :ref:`modem-opus-snmp`
    * - 180
      - ``ldd-drawtoy``
      - :ref:`modem-ldd-drawtoy`
    * - 188
      - ``opus-proxy``
      - :ref:`modem-opus-proxy`
    * - 701
      - ``ldd-tron``
      - :ref:`modem-ldd-tron`
    * - 999
      - ``opus-discovery``
      - :ref:`modem-opus-discovery`
    * - 1005
      - ``ldd-wireless-rs``
      - :ref:`modem-ldd-wireless-rs`
    * - 1294
      - ``ldd-ldris``
      - :ref:`modem-ldd-ldris`
    * - 3636
      - ``lms``
      - :ref:`modem-gollark-lms`
    * - 3773
      - ``opus-neural-canvas``
      -
    * - 4200
      - ``oneos-ping``
      - :ref:`modem-oneos-ping`
    * - 4201
      - ``oneos-ping-reply``
      - :ref:`modem-oneos-ping`
    * - 4202
      - ``oneos-turtle-remote``
      - :ref:`modem-oneos-turtle`
    * - 4203
      - ``oneos-turtle-remote-reply``
      - :ref:`modem-oneos-turtle`
    * - 4204
      - ``oneos-transmit-discovery``
      - :ref:`modem-oneos-transmit`
    * - 4205
      - ``oneos-transmit-discovery-reply``
      - :ref:`modem-oneos-transmit`
    * - 4206
      - ``oneos-transmit-request``
      - :ref:`modem-oneos-transmit`
    * - 4207
      - ``oneos-transmit-request-reply``
      - :ref:`modem-oneos-transmit`
    * - 4208
      - ``oneos-transmit-send``
      - :ref:`modem-oneos-transmit`
    * - 4210
      - ``oneos-door-lock-ping``
      - :ref:`modem-oneos-doorlock`
    * - 4211
      - ``oneos-door-lock-request``
      - :ref:`modem-oneos-doorlock`
    * - 4212
      - ``oneos-door-lock-request-reply``
      - :ref:`modem-oneos-doorlock`
    * - 4242
      - ``milo-remote``
      -
    * - 5222
      - ``opus-pickup-remote``
      -
    * - 5731
      - ``jackmac-rawshell-control``
      - :ref:`modem-jackmac-rawshell`
    * - 5900
      - ``opus-vnc``
      - :ref:`modem-opus-vnc`
    * - 5901
      - ``opus-svnc``
      - :ref:`modem-opus-vnc`
    * - 5902
      - ``opus-mirror``
      -
    * - 8366
      - ``ldd-progdor2``
      - :ref:`modem-ldd-progdor2`
    * - 11000
      - ``ldd-enchat3``
      - :ref:`modem-ldd-enchat3`
    * - 31415
      - ``gollark-rift``
      - :ref:`modem-gollark-rift`
    * - 60000
      - ``opus-message``
      -
    * - 62381
      - ``potatos-lancmd``
      - :ref:`modem-gollark-lancmd`
    * - 65530
      - ``ldd-brednet``
      -
    * - 65533
      - ``rednet-repeat``
      - :ref:`rednet`
    * - 65534
      - ``gps``
      - :ref:`modem-gps`
    * - 65535
      - ``rednet-broadcast``
      - :ref:`rednet`

.. todo::

    Describe LDDestroier's brednet protocol, and see if it doesn't actually fit
    on the current page.

Remarks:

* OneOS has an explicit comment about using channels between 4200 and 4300.
  It also has an ``Wireless.Channels.Ignored`` set to 4299, which is not used
  within the code.
* OPUS OS uses ports in the :math:`[16384, 32767]` range for connections.

Some references for this work are:

* The `ComputerCraft Forums`_ and `ComputerCraft: Tweaked Forums`_,
  which hosts many project pages for the mods (*links to the dedicated
  topics are usually provided; if they're not, feel free to contribute!*).
* `Lupus590's ComputerCraft Modem Channel Usage`_, a page that aims at
  referencing existing protocols and best practices for ComputerCraft
  networking.

In the following chapters, we'll go more in depth on port protocols.

.. toctree::
    :maxdepth: 2

    modem-protocols/gps
    modem-protocols/oneos
    modem-protocols/opus
    modem-protocols/ldd
    modem-protocols/jackmac
    modem-protocols/gollark

.. _repeat command: ttps://github.com/cc-tweaked/CC-Tweaked/blob/4be0b15afa9d81baf7859cf6c04c048896d7ccdf/src/main/resources/data/computercraft/lua/rom/programs/rednet/repeat.lua
.. _EtherTypes: https://fr.wikipedia.org/wiki/EtherType
.. _rednet reference: https://tweaked.cc/module/rednet.html
.. _rednet.open: ttps://github.com/cc-tweaked/CC-Tweaked/blob/052cf8ee7d3ca3da58533ca3ce55d5a28817515d/src/main/resources/data/computercraft/lua/rom/apis/rednet.lua#L45
.. _rednet.send: https://tweaked.cc/module/rednet.html#v:send
.. _rednet.run: ttps://github.com/cc-tweaked/CC-Tweaked/blob/1316d6a3c906eb1dcc0323982b46f655777ee628/src/main/resources/data/computercraft/lua/rom/apis/rednet.lua#L354
.. _IDAssigner.getNextID: ttps://github.com/cc-tweaked/CC-Tweaked/blob/e52d98ad8bcce5b85647921a7aa12168451a1ac6/src/main/java/dan200/computercraft/shared/util/IDAssigner.java#L39
.. _Java Primitive Types and Values: https://docs.oracle.com/javase/specs/jls/se7/html/jls-4.html#jls-4.2
.. _TRON: https://forums.computercraft.cc/index.php?topic=84.msg298#msg298
.. _Lupus590's ComputerCraft Modem Channel Usage: https://docs.google.com/document/d/1RVmTqPdqkTPD2aT-KBksnrBIK1hn-1Wazo7sijubeNU/edit
.. _Enchat3: https://www.computercraft.info/forums2/index.php?/topic/29724-enchat-30-encrypted-decentralized-colorized-pictochat-capable-skynet-enabled-chatbox-compatible-chat/
.. _brednet: https://github.com/LDDestroier/CC/blob/master/brednet.lua
.. _ComputerCraft Forums: https://www.computercraft.info/forums2/
.. _`ComputerCraft: Tweaked Forums`: https://forums.computercraft.cc/
.. _CryptoNet: https://github.com/SiliconSloth/CryptoNet
