.. _video-devices:

Video devices
=============

A video device for thox is a device for displaying information and
graphics. Most video devices for thox are actually `text displays`_.
They can be accessed by thox sometimes directly, other times through an
other device, e.g. a GPU_ in OpenComputers.

Note that while a video device might not be considered a device by
the native system, thox abstracts it as is; for example, the internal
display in ComputerCraft is considered a device on the built-in bus.

With ComputerCraft monitors, the expected character encoding for displaying
glyphs is the custom encoding; see :ref:`computercraft-encoding`.

For reference, in ComputerCraft, the monitor API is described through the
`term.Redirect`_ class. The internal display API is available through the
`term`_ module, while monitor devices implement the `monitor`_ API.

.. todo::

    Describe capabilities, dimensions, character set, etc.
    16-bit palette, definable?

.. todo::

    Graphics modes. See more about `Graphics Mode in CraftOS PC
    <https://www.craftos-pc.cc/docs/gfxmode>`_, with CraftOS PC emulating
    the text mode (ComputerCraft's reference mode) as graphics mode 0
    while providing other APIs.

    `BLittle API`_ allows to use characters in the 80-9F range to draw
    more precise pixels.

.. _text displays: https://en.wikipedia.org/wiki/Text_display
.. _term: https://tweaked.cc/module/term.html
.. _term.Redirect: https://tweaked.cc/module/term.html#ty:Redirect
.. _monitor: https://tweaked.cc/peripheral/monitor.html
.. _GPU: https://ocdoc.cil.li/component:gpu
.. _BLittle API: http://www.computercraft.info/forums2/index.php?/topic/25354-cc-176-blittle-api/
