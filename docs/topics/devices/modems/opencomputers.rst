OpenComputers modems
====================

.. sidebar:: OpenComputers modems

    .. list-table::
        :widths: 2 8

        * - addressing
          - yes
        * - max_open
          - none
        * - addr_fmt
          - component uuid
        * - port_fmt
          - :math:`[0, 65535]`
        * - msg_fmt
          - sequence of 1 to 8 of nil, boolean, number or string

OpenComputers adds network cards which you can add to computers for
network connectivity. They actually have a maximum packet size. In order
to calculate the packet size from the given sequence as the message format:

* Take the product of the number of members in the sequence and two.
  This is because each member of the sequence adds two bytes of overhead.
* Then, for each number of the sequence:

  - If it is a ``nil`` or a boolean, add four.
  - Otherwise, if it is an empty string, add one.
  - Otherwise, if it is a string, add the number of bytes in the string.
  - Otherwise, if it is a number, add eight.

Example sequences are the following:

* ``"currentStatus", 300``: the number of bytes of the packet is
  :math:`2 * 2 + 13 + 8 = 25`
* ``nil``: the number of bytes of the packet is :math:`2 * 1 + 4 = 6`

.. todo::

    There is plenty more to say about OpenComputers modems.
    Also, might be worth to try and get the maximum packet size into
    the general modem abstraction.

See the `OpenComputers modem API`_ for more information.

.. _OpenComputers modem API: https://ocdoc.cil.li/component:modem
