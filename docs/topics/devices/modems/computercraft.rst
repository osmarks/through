ComputerCraft modems
====================

.. sidebar:: ComputerCraft modems

    .. figure:: computercraft-modem-types.png
        :alt: Modem types represented in ComputerCraft.

        *From left to right: wired modem, wireless modem, ender modem.*

    .. list-table::
        :widths: 2 8

        * - addressing
          - no
        * - max_open
          - 128
        * - port_fmt
          - :math:`[0, 65535]`
        * - msg_fmt
          - any string, number, boolean or table

ComputerCraft adds three different kinds of modems:

* **wired modems**, which allows a computer to be connected to another
  through wires.
* **wireless modems**, which allows a computer to be connected to others
  without wires with a limited range depending on the weather.
* **ender modems** are similar to wireless modems (in fact, ComputerCraft
  recognizes them as “advanced” wireless modems), except that for a given
  channel, they can communicate with all wireless and ender modems in all
  dimensions, and listen to all communications from wireless and
  ender modems from all dimensions; see `WirelessNetwork.tryTransmit`_
  (where ``interdimensional`` is set for packets sent by ender modems,
  and ``receiver.isInterdimensional`` is true if the receiver is an ender
  modem).

Note that what ComputerCraft wiki calls dimensions corresponds to what
Mojang mappings calls worlds (hence the technical name
``net.minecraft.world.World``).

All modems work the same: datagrams are sent on ports whose numbers vary
between 0 and 65535 (see `modem.open`_).
Through such a modem, computers can send a message from any arbitrary port,
and receive messages on up to 128 simultaneously opened ports at once.

.. note::

    The opened port limit can be pushed further by using several modems on
    a computer; e.g. up to 256 ports can be opened when two wireless modems
    are connected.

Messages are Lua strings, numbers, or tables; metatables are not transmitted
along the way, a byproduct of using the ``@LuaFunction`` decorator from
ComputerCraft.

Any message on the given modems also send along the pipe the “reply channel”,
which for Rednet-enabled devices corresponds to the source channel. Note that
this can be set by the user program when sending a message and does not
correspond to a MAC address or anything, therefore cannot be used as a
reliable source port (see Rednet).

When receiving a message, in the produced ``modem_message`` event in CraftOS,
wireless and ender modems within the same dimensions include the distance from
the emitting device; this specificity is used on some protocols,
such as the GPS one (see :ref:`modem-gps`). Note that the distance is
calculated not using the position of the modem themselves, but:

* When the modem is plugged on the side bus (see
  :ref:`bus-computercraft-side`), the position of the center of the computer
  directly.
* When the modem is plugged on the wired bus (see
  :ref:`bus-computercraft-wired`), the position of the center of the wired
  modem that is directly aside the modem receiving the message.

The distance itself is calculated:

* On wireless modems, the distance between the two positions determined
  using the method described above.
* On wired modems, the number of cables and wired modems (including those
  used to send and receive the message on both sides) traversed by the
  message.

.. warning::

    If you're running a version of ComputerCraft before 1.53, tables
    are not supported as valid message types.

.. note::

    Although modem as blocks use integer coordinates, even turtles when they
    move (positions update immediately to the new block position), some
    modems can have decimal positions, most importantly pocket computers,
    linked to the player's coordinates.

    Classic GPS hosts round the coordinates with a .01 precision.

Basic wireless modems have a limit on how far they can send modem messages.
The formula for determining this limit, named :math:`D_{max}` and represented
in blocks, is the following:

.. math::

    \begin{cases}
    D_{max} = R_{low} + (y - 96) * ((R_{high} - R_{low}) / (Y_{max} - 1 - 96) \text{ if $y > 96$} \\
    D_{max} = R_{low} \text{ if $y <= 96$}
    \end{cases}

Where the variables are the following:

* :math:`y` is the modem's y coordinate.
* :math:`Y_{max}` is the maximum Y coordinate, equal to 256 in Minecraft 1.16.
* :math:`R_{low}` represents the range of wireless modems at "low altitude",
  i.e. at :math:`y = 96` and below; it is configurable, but is of 64 in both
  clear weather and storm.
* :math:`R_{high}` represents the range of wireless modems at "high altitude",
  i.e. at :math:`y = Y_{max}`; it is configurable, but is of 384 in both
  clear weather and storm.

.. note::

    In legacy ComputerCraft, the values were the following:

    .. list-table::
        :widths: 5 5 5
        :header-rows: 1

        * - Weather
          - Range at low alt.
          - Range at high alt.
        * - Clear
          - 64
          - 384
        * - Storm
          - 16
          - 96

.. figure:: computercraft-wireless-range.png
    :align: center
    :alt: Wireless range evolution.

A vizualization of the range depending on the current y coordinate,
with :math:`Y_{max}` being defined to 256, its current value.
The values taken for clear and stormy weathers are those of
legacy ComputerCraft.

See `WirelessModemPeripheral.getRange`_, `Config.Config`_ and
`ComputerCraft.modemRange`_ for reference.

In versions of ComputerCraft prior to 1.80pr1.3, wired modems/network cables
have a limit of 256 cables.

.. _modem.open: https://tweaked.cc/peripheral/modem.html#v:open
.. _WirelessModemPeripheral.getRange: ttps://github.com/cc-tweaked/CC-Tweaked/blob/9d1ee6f61db478f364afa15a439a123ab21175ae/src/main/java/dan200/computercraft/shared/peripheral/modem/wireless/WirelessModemPeripheral.java#L32
.. _Config.Config: ttps://github.com/cc-tweaked/CC-Tweaked/blob/58054ad2d1a9a967f1ef5dc981da5db938bb2c71/src/main/java/dan200/computercraft/shared/Config.java#L206
.. _ComputerCraft.modemRange: ttps://github.com/cc-tweaked/CC-Tweaked/blob/e4b0a5b3ce035eb23feb4191432fc49af5772c5b/src/main/java/dan200/computercraft/ComputerCraft.java#L60
.. _WirelessNetwork.tryTransmit: ttps://github.com/cc-tweaked/CC-Tweaked/blob/1316d6a3c906eb1dcc0323982b46f655777ee628/src/main/java/dan200/computercraft/shared/peripheral/modem/wireless/WirelessNetwork.java#L64
