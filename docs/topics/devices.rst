.. _buses-and-devices:

Buses and devices
=================

The capabilities of any computer are extended using **devices**.
Devices are internal or external devices attached to a computer and offer
additional functionalities through Lua interfaces, usually Lua **methods**.

There are several types of devices:

* Built-in devices, which are usually integrated through native Lua
  APIs. For example, the native term_ API in CC:T abstracts
  the terminal device.
* Hotplug devices, usually named peripherals, which are integrated
  through a generic API such as the peripheral_ API in CC:T.

For example, a wired modem, recognized as a peripheral in CC:T, has the
following methods available, documented in modem_:

.. image:: devices-wmfunc.png
    :alt: An advanced computer listing the methods available in the wired
          modem's interface.

Some devices are:

* Disk drives, for reading and writing floppies.
* Video devices, such as monitors or terminals, for displaying information
  and graphics; see :ref:`video-devices`.
* Printers, for printing information such as :download:`a newspaper
  <devices-printer-stissue.png>`.
* Modems, for networking with other computers; see :ref:`modem-devices`.
* Speakers, for emitting sound to the real world.
* And *many other things*!

Devices are accessible through **buses**. Buses are the link between
computers and devices. On every bus, every device has an **address**
which is the device's identifier on this bus, used for calling a
method on this device through this bus.

Buses
-----

For more information about specific buses, consult the following sections.

.. toctree::
    :maxdepth: 1

    buses/core
    buses/computercraft/side
    buses/computercraft/wired
    buses/opencomputers

Devices
-------

For more information about specific devices, consult the following sections.

.. toctree::
    :maxdepth: 1

    devices/modems
    devices/video

.. _term: https://tweaked.cc/module/term.html
.. _peripheral: https://tweaked.cc/module/peripheral.html
.. _modem: https://tweaked.cc/peripheral/modem.html
