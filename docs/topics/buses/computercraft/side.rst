.. _bus-computercraft-side:

ComputerCraft side bus
----------------------

Peripherals can be attached to the computer directly, on one side. There are
six sides on basic computers: ``front``, ``back``, ``left``, ``right``,
``top`` and ``bottom``. For other devices, it's basically a derivation of this:

* Pocket computers can only have one peripheral (usually a wireless modem),
  which will be considered as equipped at the back.
* Turtles can have up to two peripherals, on the left and on the right.
  They can equip and unequip a peripheral (or a tool) without any player
  help if necessary. Tools are not considered as peripherals.

These devices and calls are available in the CraftOS API through the
peripheral_ API. Note however that the default API accessible to any
program also checks for devices on the ComputerCraft wired bus; the native
interface needs to be used for effective isolation.

.. _peripheral: https://tweaked.cc/module/peripheral.html
