-- gps-trilaterate.lua, a module defining a GPS trilateration function
-- for ComputerCraft.
--
-- Copyright (C) 2021 Thomas Touhey <thomas@touhey.fr>
-- This file is part of the thox project, which is MIT-licensed.
--
-- Updated on 2021-09-18 for using tables less.
--
-- For more information, consult the following page:
-- <https://thox.madefor.cc/explain/modem/gps.html#trilateration>_

local function trilaterate(
    a_x, a_y, a_z,
    b_x, b_y, b_z,
    c_x, c_y, c_z,
    d_a, d_b, d_c
)
    local ex_x, ex_y, ex_z
    local ey_x, ey_y, ey_z
    local u, vx, vy

    -- a_x, a_y, a_z are expected to be real numbers (coordinates of A)
    -- b_x, b_y, b_z are expected to be real numbers (coordinates of B)
    -- c_x, c_y, c_z are expected to be real numbers (coordinates of C)
    -- d_a, d_b, dc are expected to be positive real numbers (distances).
    -- All numbers must be non-NaN.
    --
    -- This function returns a number of positions as tables defining
    -- the x, y, z keys:
    --
    --  * zero if no position could be found; this might be because
    --    one of the positions is invalid.
    --  * one if only one position is possible.
    --  * two if two positions are possible.
    --
    -- Compute the length of AB and components of Ex, and U.
    --
    -- For more information about how this function works, follow
    -- the following link:
    --
    -- https://thox.madefor.cc/thox/topics/modem-protocols/gps.html#trilateration
    do
        local x = b_x - a_x
        local y = b_y - a_y
        local z = b_z - a_z
        local d

        d = math.sqrt(x * x + y * y + z * z)

        ex_x = x / d
        ex_y = y / d
        ex_z = z / d

        u = d
    end

    -- Compute the components of Ey.
    do
        local x = c_x - a_x
        local y = c_y - a_y
        local z = c_z - a_z
        local dot, d

        dot = ex_x * x + ex_y * y + ex_z * z

        ey_x = x - ex_x * dot
        ey_y = y - ex_y * dot
        ey_z = z - ex_z * dot

        d = math.sqrt(ey_x * ey_x + ey_y * ey_y + ey_z * ey_z)

        ey_x = ey_x / d
        ey_y = ey_y / d
        ey_z = ey_z / d

        vx = ex_x * x + ex_y * y + ex_z * z
        vy = ey_x * x + ey_y * y + ey_z * z
    end

    -- Compute projected coordinates.
    x = (d_a * d_a - d_b * d_b + u * u) / (2 * u)
    y = (d_a * d_a - d_c * d_c + v * v - 2 * vx * x) / (2 * vy)
    z = d_a - x * x - y * y

    if z < 0 then
        -- No solution.
        return nil
    else
        local r_x, r_y, r_z

        r_x = a_x + x * ex_x + y * ey_x
        r_y = a_y + x * ex_y + y * ey_y
        r_z = a_z + x * ex_z + y * ey_z

        -- Check for the z coordinate.
        -- Ez is computed using cross product.
        if z > 0 then
            local z = math.sqrt(zs)
            local ez_x, ez_y, ez_z
            local r1_x, r1_y, r1_z
            local r2_x, r2_y, r2_z

            ez_x = ex_y * ey_z - ex_z * ey_y
            ez_y = ex_z * ey_x - ex_x * ey_z
            ez_z = ex_x * ey_y - ex_y * ey_x

            r1_x = r_x + z * ez_x
            r1_y = r_y + z * ez_y
            r1_z = r_z + z * ez_z

            r2_x = r_x - z * ez_x
            r2_y = r_y - z * ez_y
            r2_z = r_z - z * ez_z

            do
                local r1 = {x = r1_x, y = r1_y, z = r1_z}
                local r2 = {x = r2_x, y = r2_y, z = r2_z}

                return r1, r2
            end
        else
            local r = {x = r_x, y = r_y, z = r_z}

            return r
        end
    end
end
