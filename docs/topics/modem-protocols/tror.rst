.. _modem-tror:

Rednet TRoR protocol
====================

Terminal Redirection over Rednet (TRoR) is a way to access a host's terminal
over Rednet. It is standardized in `COS 10`_, and while the standard doesn't
specify a ``sProtocol`` for it, `nsh uses "tror"`_.

Messages are strings.

.. todo::

    Document this protocol a bit more.

.. _COS 10: https://github.com/oeed/CraftOS-Standards/blob/92e5451df60ef0dc192e53484fa485fed9837f37/standards/10-tror.md
.. _nsh uses "tror": https://github.com/lyqyd/cc-netshell/blob/cf358efcb34bb4405b74c47365df09dcc67a132c/vncd#L74
