.. _modem-ldd:

LDDestroier's protocols
=======================

:ref:`player-lddestroier` is a member who has done notable programs such as:

* `PAIN`_, a picture editor supporting most formats known to ComputerCraft.
* `TRON`_, a Lua adaptation running on ComputerCraft computers of the
  game popularized by the movie of the same name.
* `Enchat 3`_, an in-game chat program with end-to-end encryption.
* `Progdor2`_, a file archiver and transfer program.

Most of their programs are available in `a single repository <CC_>`_.
In this document, you'll be able to find out about the protocols used by
their software.

.. _modem-ldd-wireless-rs:

Wireless redstone
-----------------

This protocol is a port protocol running on port 1005. It is a protocol that
has two components:

* Requests to control the redstone logical outputs on given directions
  (left to right, front to back, top to bottom).
* Events when updates on redstone logical inputs occur.

.. todo:: Describe the program and the protocol.

See `wireless-rs.lua`_ for reference.

.. _modem-ldd-drawtoy:

drawtoy protocol
----------------

.. todo:: Describe the program and the protocol.

See `drawtoy.lua`_ for reference.

.. _modem-ldd-progdor2:

Progdor2 protocol
-----------------

.. todo:: Describe the program and the protocol.

See `progdor2.lua`_ for reference.

.. _modem-ldd-enchat3:

Enchat3 protocol
----------------

.. todo:: Describe the program and the protocol.

See `enchat3.lua`_ for reference.

.. _modem-ldd-ldris:

LDRis protocol
--------------

LDRis is a Tetris clone.

.. todo:: Describe the program and the protocol.

See `ldris.lua`_ for reference.

.. _modem-ldd-tron:

TRON protocol
-------------

.. todo:: Describe the program and the protocol.

See `tron.lua`_ for reference.

.. _CraftBang: https://forums.computercraft.cc/index.php?topic=103.msg418
.. _TRON: https://forums.computercraft.cc/index.php?topic=84
.. _PAIN: https://www.computercraft.info/forums2/index.php?/topic/26418-pain-nfpnftbltgifucg-support-block-characters-undo-buffer-fill-tool-and-an-infinite-scrollable-canvas/
.. _Enchat 3: https://forums.computercraft.cc/index.php?topic=74
.. _Progdor2: https://forums.computercraft.cc/index.php?topic=127
.. _CC: https://github.com/LDDestroier/CC
.. _wireless-rs.lua: https://github.com/LDDestroier/CC/blob/a6cb3359554575d9c10af6db84bd33494447571f/wireless-rs.lua
.. _drawtoy.lua: https://github.com/LDDestroier/CC/blob/9cd8f7da4b143f4478a955afdc9db3b072e5b7d0/drawtoy.lua
.. _progdor2.lua: https://github.com/LDDestroier/CC/blob/c904ce25e9f9ee66cb9ad25e9870b67d280d52e9/progdor2.lua
.. _enchat3.lua: https://github.com/LDDestroier/enchat/blob/e3df898259e73867884389fdde2ac2a3a87c633a/enchat3.lua
.. _ldris.lua: https://github.com/LDDestroier/CC/blob/547d279838da394e2519f4524a12ffa4ab5489cc/ldris.lua
.. _tron.lua: https://github.com/LDDestroier/CC/blob/d5dbc125fc94b8bc6da96e44efd3b68ee2017ade/tron.lua
