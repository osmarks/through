.. _glossary:

Glossary
========

Note that this glossary is only about environments in which thox run.
It is not a general computer science glossary; some approximations
used here are not true in other contexts.

.. glossary::

    bus
        A medium that allows communicating with devices using
        addresses and events.

        See :ref:`buses-and-devices` for more information.

    CC
        Refers to the original ComputerCraft mod by :ref:`player-dan200`.

        See :ref:`computers` for more information.

    CC:T
        Refers to ComputerCraft: Tweaked.

        See :ref:`computers` for more information.

    character encoding
        An algorithm to represent a sequence of characters over a sequence
        of bytes or characters. This algorithm must be bijective.
        Transforming the former into the latter is called "encoding",
        transforming the latter into the former is called "decoding".

        See :ref:`character-encodings` for more information.

    computer
        A computer is a device able to run thox.

        See :ref:`computers` for more information.

    console
        Alternative word for "terminal".

    device
        An element which the computer can interact with through a bus.

        See :ref:`buses-and-devices` for more information.

    event
        An event produced by a peripheral signalling real world activity
        to which a computer can react. Also called signal.

        See :ref:`buses-and-devices` for more information.

    host
        A device on a network that can send and receive messages
        through a modem. Hosts can be computers or other devices.

        See :ref:`modem-devices` for more information.

    modem
        A kind of peripheral that can be used by computers to send and
        receive messages on given channels on given networks.

        See :ref:`modem-devices` for more information.

    monitor
        A peripheral which allows a computer to display characters, and
        register input events.

        See :ref:`video-devices` for more information.

    peripheral
        An external, hot-plug device.

        See :ref:`buses-and-devices` for more information.

    screen
        Alternative word for "monitor".

    terminal
        Text display.

        See :ref:`video-devices` for more information.

    turtle
        A computer equipped with world interaction abilities, in the
        train of thoughts of the turtle in the `Logo programming language`_:
        move, dig, place, drop, attack, inspect, and so on.
        Also called robot or drone.

.. _Logo programming language:
    https://en.wikipedia.org/wiki/Logo_(programming_language)
